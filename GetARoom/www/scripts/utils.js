﻿function openDatePicker() {
    var options = {
        date: new Date(),
        mode: 'date'
    };

    function onSuccess(date) {
        alert('Selected date: ' + date);
    }

    function onError(error) { // Android only
        alert('Error: ' + error);
    }

    datePicker.show(options, onSuccess, onError);

}