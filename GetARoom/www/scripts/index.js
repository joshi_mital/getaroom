﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";
    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);
        $(".searchIcon").css("height", $(".ui-input-text").height());
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
    };
    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };
})();

$(document).on("pagecreate", "#homePage", function () {
    
    $("#seeDealsBtn").click(function () {
        $.mobile.changePage("pages/newPage.html");
    });
    $(".searchBtnBar").click(function () {
        $("#myModal").modal();
    });
    $("#checkInDateBtn").click(function () {
        openDatePicker();
    });
    $("#checkOutDateBtn").click(function () {
        openDatePicker();
    });
});

$(document).on("pageshow", "#newPage", function () {
    var data = [1, 2, 3];
    $("#tmplScript").tmpl(data).appendTo("#tmplDiv");
});

$(document).on("pagecreate", "#hotellistonecolumn", function () {
    var listicon = $("#listicon");
    var gridicon = $("#gridicon");
    var listview = $("#listview");
    var gridview = $("#gridview");
    $('.container').rating();
    $(listicon).click(function () {
        gridview.hide();
        listview.show();
    });

    $(gridicon).click(function () {
        gridview.show();
        listview.hide();
    });
});
